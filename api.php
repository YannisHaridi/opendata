<?php
class api {

  function print_select($url){
    $page = file_get_contents($url,true);
    $contents = utf8_encode($page);
    $results = json_decode($contents,true);
    foreach ($results["facet_groups"][0]["facets"] as $key => $value) {
      $formation = $value["name"];
      $code = $value["count"];
      echo "<option value='".$formation."'>".$formation."</option>";
    }
  }
  static function construct_url($type_formation,$niveau,$departement,$domaine) { //permet d'utiliser uniquement la facet du select si ce dernier est non vide
    $chaine = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=100";
    if (!empty($type_formation)) { //si le select n est pas vide
      $chaine .= "&facet=diplome_lib&refine.diplome_lib=" . urlencode($type_formation); //on ajoute la facet a l'url
    }
    if (!empty($niveau)) {
      $chaine .= "&facet=niveau_lib&refine.niveau_lib=" . urlencode($niveau); 
    }
    if (!empty($departement)) {
      $chaine .= "&facet=dep_etab_lib&refine.dep_etab_lib=" . urlencode($departement);
    }
    if (!empty($domaine)) {
      $chaine .= "&facet=sect_disciplinaire_lib&refine.sect_disciplinaire_lib=" . urlencode($domaine);
    }
    return $chaine."&apikey=4ce105730e4babf31e68688260a2fc625849644b0a81b63ccc482345";
    }
}
 ?>
