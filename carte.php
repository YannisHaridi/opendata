<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
  integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
  crossorigin=""/>
  <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
  integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
  crossorigin=""></script>
    <meta charset="utf-8">
    <link href="style.css" rel="stylesheet">
    <title>FORMASUP</title>
  </head>
  <body>
    <div id="page">
<div class="flag">
  <h1 class="main_title"><a href="index.php">FormaSup</a></h1>


  <br>
    </div>
    <ul>
  <li>
      <div class="wrapper">
        <a class="cta" href="formation.php" id="bloc1">
          <span>Trouver une formation</span>
          <span>
            <svg width="66px" height="43px" viewBox="0 0 66 43" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
              <g id="arrow" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <path class="one" d="M40.1543933,3.89485454 L43.9763149,0.139296592 C44.1708311,-0.0518420739 44.4826329,-0.0518571125 44.6771675,0.139262789 L65.6916134,20.7848311 C66.0855801,21.1718824 66.0911863,21.8050225 65.704135,22.1989893 C65.7000188,22.2031791 65.6958657,22.2073326 65.6916762,22.2114492 L44.677098,42.8607841 C44.4825957,43.0519059 44.1708242,43.0519358 43.9762853,42.8608513 L40.1545186,39.1069479 C39.9575152,38.9134427 39.9546793,38.5968729 40.1481845,38.3998695 C40.1502893,38.3977268 40.1524132,38.395603 40.1545562,38.3934985 L56.9937789,21.8567812 C57.1908028,21.6632968 57.193672,21.3467273 57.0001876,21.1497035 C56.9980647,21.1475418 56.9959223,21.1453995 56.9937605,21.1432767 L40.1545208,4.60825197 C39.9574869,4.41477773 39.9546013,4.09820839 40.1480756,3.90117456 C40.1501626,3.89904911 40.1522686,3.89694235 40.1543933,3.89485454 Z" fill="#FFFFFF"></path>
                <path class="two" d="M20.1543933,3.89485454 L23.9763149,0.139296592 C24.1708311,-0.0518420739 24.4826329,-0.0518571125 24.6771675,0.139262789 L45.6916134,20.7848311 C46.0855801,21.1718824 46.0911863,21.8050225 45.704135,22.1989893 C45.7000188,22.2031791 45.6958657,22.2073326 45.6916762,22.2114492 L24.677098,42.8607841 C24.4825957,43.0519059 24.1708242,43.0519358 23.9762853,42.8608513 L20.1545186,39.1069479 C19.9575152,38.9134427 19.9546793,38.5968729 20.1481845,38.3998695 C20.1502893,38.3977268 20.1524132,38.395603 20.1545562,38.3934985 L36.9937789,21.8567812 C37.1908028,21.6632968 37.193672,21.3467273 37.0001876,21.1497035 C36.9980647,21.1475418 36.9959223,21.1453995 36.9937605,21.1432767 L20.1545208,4.60825197 C19.9574869,4.41477773 19.9546013,4.09820839 20.1480756,3.90117456 C20.1501626,3.89904911 20.1522686,3.89694235 20.1543933,3.89485454 Z" fill="#FFFFFF"></path>
                <path class="three" d="M0.154393339,3.89485454 L3.97631488,0.139296592 C4.17083111,-0.0518420739 4.48263286,-0.0518571125 4.67716753,0.139262789 L25.6916134,20.7848311 C26.0855801,21.1718824 26.0911863,21.8050225 25.704135,22.1989893 C25.7000188,22.2031791 25.6958657,22.2073326 25.6916762,22.2114492 L4.67709797,42.8607841 C4.48259567,43.0519059 4.17082418,43.0519358 3.97628526,42.8608513 L0.154518591,39.1069479 C-0.0424848215,38.9134427 -0.0453206733,38.5968729 0.148184538,38.3998695 C0.150289256,38.3977268 0.152413239,38.395603 0.154556228,38.3934985 L16.9937789,21.8567812 C17.1908028,21.6632968 17.193672,21.3467273 17.0001876,21.1497035 C16.9980647,21.1475418 16.9959223,21.1453995 16.9937605,21.1432767 L0.15452076,4.60825197 C-0.0425130651,4.41477773 -0.0453986756,4.09820839 0.148075568,3.90117456 C0.150162624,3.89904911 0.152268631,3.89694235 0.154393339,3.89485454 Z" fill="#FFFFFF"></path>
              </g>
            </svg>
          </span>
        </a>
      </div></li></ul>
  <form method="post" action="formation_search.php">
    <input class="search" name="value" type="text" placeholder="Cherchez une formation ou un établissement...">
    <input class="search-btn" type="submit" value="Rechercher" name="search_btn">
  </form>
  <?php
  require("back.php");
  $libelle=$_POST['libelle'];
  $etab=$_POST['etab'];
  $dep=$_POST['dep'];
  $niveau=$_POST['niveau'];
  $effectif=$_POST['effectif'];
  $no_map="Carte indisponible pour cet etablissement";
  $code = $_POST['code'];

//on initialise le nombre de clic
  $nb_clic=back::get_clic($code,$libelle,$etab,$dep); //on recupere le nombre de clics
  back::add_clic($nb_clic,$code); //on ajoute un nouveau clic etant donné qu'on a chargé la page
  $nb_clic = back::get_clic($code,$libelle,$etab,$dep); //puis on recupere la nouvelle valeur du nombre de clic


  if(!empty($_POST['id'])){ //importe l'id de la formation de la premiere api
  $id = $_POST['id'];
  $url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-etablissements-enseignement-superieur&rows=20&sort=uo_lib&facet=uai&refine.uai=".$id; //trouve la bonne formation à partir de l'id (chaque formation de la premiere api est associée a celle de la deuxieme via cet id)
  $page = file_get_contents($url,true);
  $contents = utf8_encode($page);
  $results = json_decode($contents,true);
  foreach ($results["records"] as $value) {
      $x = $value["fields"]["coordonnees"][0];
      $y = $value["fields"]["coordonnees"][1];
      $no_map="";
  }
}
?>
<br>
<div class="global">
<div class="descriptif">
<h2><?php echo $libelle ?></h2>
<hr>
<?php echo $nb_clic ?><img src="eye.svg" height="14px" width="31px"/>
<h3>Etablissement</h3>
<?php echo $etab ?>
<br>
<h3>Departement</h3>
<?php echo $dep ?>
<h3>Cycle</h3>
<?php echo $niveau ?>
<h3>Effectif</h3>
<?php echo $effectif ?> étudiants
</div>
<div class="map">
 <div id="mapid"></div>
 <p id="no_map"><?php echo $no_map?> </p>
 <script>
 var mymap = L.map('mapid').setView([47.1239268, 3.4203712], 6);
 L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
}).addTo(mymap);
 <?php
 echo "var marker = L.marker([".$x.",".$y."]).addTo(mymap)"; //affiche le marqueur avec les coordonnées x,y
 ?>
</script>
</div>
</div>
</div>
  </body>
  <footer>
    Droits reservés - France 2020 (<a href="https://bitbucket.org/YannisHaridi/opendata/src/master/">Pour en savoir plus</a>)
  </footer>
</html>
