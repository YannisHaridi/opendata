<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="style.css" rel="stylesheet">
    <title>FORMASUP</title>
    <?php 
    require ('api.php');
    ?>
  </head>
  <body>
    <div class="flag">
  <h1 class="main_title"><a href="index.php">FormaSup</a></h1>
    </div>
<form method="post" action="formation_search.php">
    <input class="search" name="value" type="text" placeholder="Cherchez une formation ou un établissement...">
    <input class="search-btn" type="submit" value="Rechercher" name="search_btn">
  </form>
  <br>
  <h2>Entrez vos critères pour que l'on trouve les formations idéales pour vous ! </h2>
<form method="post" action="formation.php">
<div class="select_div">
<select name="type_formation" class="select">
    <option value="c1" selected="selected" disabled="disabled">Type de formation</option>
    <?php
    $url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=0&sort=-rentree_lib&facet=diplome_lib&refine.rentree_lib=2017-18&apikey=4ce105730e4babf31e68688260a2fc625849644b0a81b63ccc482345";
    api::print_select($url);
        ?>
            </select>
<select name="niveau" class="select">
    <option value="c1" selected="selected" disabled="disabled">Niveau</option>
    <?php
    $url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=0&sort=-rentree_lib&facet=niveau_lib&apikey=4ce105730e4babf31e68688260a2fc625849644b0a81b63ccc482345";
    api::print_select($url);
        ?>
            </select>
<select name="departement" class="select">
    <option value="c1" selected="selected" disabled="disabled">Département</option>
    <?php
    $url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=0&sort=-rentree_lib&facet=dep_etab_lib&apikey=4ce105730e4babf31e68688260a2fc625849644b0a81b63ccc482345";
    api::print_select($url);
        ?>
            </select>
    <select name="domaine" class="select">
    <option value="c1" selected="selected" disabled="disabled">Domaine</option>
    <?php
    $url = "https://data.enseignementsup-recherche.gouv.fr/api/records/1.0/search/?dataset=fr-esr-principaux-diplomes-et-formations-prepares-etablissements-publics&rows=0&sort=-rentree_lib&facet=sect_disciplinaire_lib&refine.rentree_lib=2017-18&apikey=4ce105730e4babf31e68688260a2fc625849644b0a81b63ccc482345";
    api::print_select($url);
        ?>
            </select>
</div>
<br>
<input type="submit" name = "valider" value="Valider">

</form>
<?php
if(!empty($_POST['type_formation']) || !empty($_POST['niveau']) || !empty($_POST['departement']) || !empty($_POST['domaine'])){
  ?>
  <TABLE>
  <tr>
    <th><b><h2>Libellé</h2></b></th>
    <th><b><h2>Etablissement</h2></b></th>
    <th><b><h2>Departement</h2></b></th>
    <th><b><h2>Niveau</h2></b></th>
    <th><b><h2>Effectif</h2></b></th>
    <th><b><h2>Informations</h2></b></th>
  </tr>
  <?php
  //on recupere toutes les informations des select
  $type_formation="";
  $niveau="";
  $departement="";
  $domaine="";
  if (isset($_POST["type_formation"])) {
    $type_formation = $_POST["type_formation"];
  }
  if (isset($_POST["niveau"])) {
    $niveau = $_POST["niveau"];
  }
  if (isset($_POST["departement"])) {
    $departement = $_POST["departement"];
  }
  if (isset($_POST["domaine"])) {
    $domaine = $_POST["domaine"];
  }
  $url = api::construct_url($type_formation,$niveau,$departement,$domaine);
  $page = file_get_contents($url,true);
  $contents = utf8_encode($page);
  $results = json_decode($contents,true);
  $counter = 0;
  foreach ($results["records"] as $value) {
    $libelle = $value["fields"]["libelle_intitule_1"];
    $etab = $value["fields"]["etablissement_lib"];
    $dep = $value["fields"]["dep_etab_lib"];
    $effectif = $value["fields"]["effectif"];
    $niveau = $value["fields"]["niveau_lib"];
    $id = $value["fields"]["etablissement"];
    $code = $value["fields"]["uucr_ins"];
    $counter = $counter + 1;
    ?>
    <tr>
      <td> <?php echo $libelle;?> </td> <!-- on crée le tableau avec les données recuperées en php -->
      <td> <?php echo $etab;?> </td>
      <td> <?php echo $dep;?> </td>
      <td> <?php echo $niveau;?> </td>
      <td> <?php echo $effectif;?> </td>
      <td><form method="post" action="carte.php">
        <input type="hidden" name="id" value="<?php echo "".$id."" ?>"></input>
        <input type="hidden" name="libelle" value="<?php echo "".$libelle."" ?>"></input>
        <input type="hidden" name="etab" value="<?php echo "".$etab."" ?>"></input>
        <input type="hidden" name="dep" value="<?php echo "".$dep."" ?>"></input>
        <input type="hidden" name="niveau" value="<?php echo "".$niveau."" ?>"></input>
        <input type="hidden" name="effectif" value="<?php echo "".$effectif."" ?>"></input>
        <input type="hidden" name="code" value="<?php echo "".$code."" ?>"></input>
        <input type="submit" name="carte"class="icon" value="">
      </form></td>
    </tr>
    <?php
    }
    ?>
    <hr>
    <h2><?php echo $counter ?> Résultats trouvés </h2>
  </TABLE>
  <?php
}
?>
  </body>
  <footer>
    Droits reservés - France 2020 (<a href="https://bitbucket.org/YannisHaridi/opendata/src/master/">Pour en savoir plus</a>)
  </footer>
</html>
